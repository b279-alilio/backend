const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({

    firstName : {
        type : String,
        required : [true, "FIRST NAME required!"]
    },
    lastName : {
        type : String,
        required : [true, "LAST NAME required!"]
    },
    email : {
        type : String,
        required : [true, "EMAIL is required!"]
    },
    password : {
        type : String,
        required: [true, "PASSWORD is required!"]
    },
    mobileNumber : {
        type : String,
        required: [true, "MOBILE NUMBER is required!"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    orderedProducts : [
        {
            product : [
                {
                    productId : {
                        type : String,
                        required : [true, "Product ID is required!"]
                    },
                    productName : {
                        type : String,
                        required : [true, "Product name is required!"]
                    },
                    quantity : {
                        type : Number,
                        required : [true, "Product quantity is required!"]
                    }
                }
            ],
            totalAmount : {
                type : Number,
                required : [true, "Total amount is required!"]
            },
            purchasedOn : {
                type : Date,
                default : new Date()
            }
        }
    ]

});


module.exports = mongoose.model("User", userSchema);
