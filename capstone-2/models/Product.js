const mongoose = require("mongoose");


const productSchema = new mongoose.Schema ({
    name : {
        type : String,
        required : [true, "PRODUCT NAME is required"]
    },
    description : {
        type : String,
        required : [true, "PRODUCT DESCRIPTION is required!"]
    },
    price : {
        type : Number,
        required : [true, "PRODUCT PRICE is required!"]
    },
    stock : {
        type : Number
    },
    image: {
        type : String,
        required : [true, "PRODUCT IMAGE is required!"]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : new Date()
    },
    userOrders : [
        {
            userId : {
                type : String,
                required : [true, "USERID is required!"]
            }
        }
    ]
});


module.exports = mongoose.model("Product", productSchema);