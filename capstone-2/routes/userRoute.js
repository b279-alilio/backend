const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


// Route for checking if user email already exists in the database.
router.post("/checkEmail", (req, res) => {
    userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {

    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});


// Route for logging in a  user
router.post("/login", (req, res) => {

    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));

});


// Route for creating order
router.post("/checkout", auth.verify, (req, res) => {

    let data = {
        userId : auth.decode(req.headers.authorization).id,
        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    userController.createOrder(data).then(resultFromController => res.send(resultFromController));

});


// Route for retrieving user details
router.get("/details", (req, res) => {

    const userData = auth.decode(req.headers.authorization).id;

    userController.userDetails(userData).then(resultFromController => res.send(resultFromController));

});


// STRETCH GOALS

// Route for setting user as admin (admin only)
router.put("/:userId/setAsAdmin", (req, res) => {

    const data = {
        userId : req.params.userId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    userController.userToAdmin(data).then(resultFromProductController => res.send(resultFromProductController));

});



module.exports = router;
