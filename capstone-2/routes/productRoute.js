const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth.js");


// Route for creating a new product (admin only)
router.post("/", auth.verify, (req, res) => {

    const data = {
        product : req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.createProduct(data).then(resultFromProductController => res.send(resultFromProductController));
});


// Route for retreieving all products (admin only)
router.get("/all", (req, res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.retrieveProduct(isAdmin).then(resultFromProductController => res.send(resultFromProductController));

});


// Route for retrieving all "Active" products
router.get("/", (req, res) => {

    productController.activeProducts().then(resultFromProductController => res.send(resultFromProductController));

});


// Route for retrieving a single product
router.get("/:productId", (req, res) => {

    productController.getSpecificProduct(req.params).then(resultFromProductController => res.send(resultFromProductController));

});


// Route for updating product information (admin only)
router.put("/:productId", auth.verify, (req, res) => {

    const data = {

        product : req.body,
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin

    }

    productController.updateProduct(data).then(resultFromProductController => res.send(resultFromProductController));

});


// Route for archiving product (admin only)
router.put("/:productId/archive", (req, res) => {

    const data = {
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(data).then(resultFromProductController => res.send(resultFromProductController));

});

// Route for unarchiving product (admin only)
router.put("/:productId/unArchive", (req, res) => {

    const data = {
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    productController.unarchiveProduct(data).then(resultFromProductController => res.send(resultFromProductController));

});



module.exports = router;
