const Product = require("../models/Product.js");


// Function for creating new product
module.exports.createProduct = (data) => {

    return Product.find({name : data.product.name}).then(result => {

        if (result.length > 0) {

            return false;

        } else if (data.isAdmin) {

            let newProduct = new Product({
                name : data.product.name,
                description : data.product.description,
                price : data.product.price,
                stock : data.product.stock,
                image : data.product.image
            });

            return newProduct.save().then((product, error) => {

                if (error) {
                    return false;
                } else {
                    return true;
                }

            });

        }

        let message = Promise.resolve("You do not have the access rights to do this action!");

        return message.then(value => {
            return value;
        });

    });
}


// Function for retrieving all products
module.exports.retrieveProduct = (isAdmin) => {

    return Product.find({}).then(result => {

        if (isAdmin === true) {

            return result;

        } else {

            return false;

        }

    });

}


// Function for retrieving all "Active" products
module.exports.activeProducts = () => {

    return Product.find({isActive : true}).then(result => {

        return result;

    });
    
}


// Funtion for retrieving a single product
module.exports.getSpecificProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {

        return result;

    });

}


// Function for updating product information (admin only)
module.exports.updateProduct = (data) => {

    if (data.isAdmin) {

        let updatedProduct = {
            name : data.product.name,
            description : data.product.description,
            price : data.product.price,
            stock : data.product.stock,
            image: data.product.image
        }

        return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {

            if (error) {

                return false;

            } else {

                return true;

            }

        });

    }

    let message = Promise.resolve("You do not have the access rights to do this action!");

    return message.then(value => {

        return value;

    });

}


// Function for archiving product (admin only)
module.exports.archiveProduct = (data) => {

    let updateProductStatus = {
        isActive : false
    }

    if (data.isAdmin) {

        return Product.findByIdAndUpdate(data.productId, updateProductStatus).then((product, error) => {

            if (error) {

                return false;

            } else {

                return true;

            }

        });

    }

    let message = Promise.resolve("You do not have the access rights to do this action!");

    return message.then(value => {

        return message;

    });

}


// Function for unarchiving product (admin only)
module.exports.unarchiveProduct = (data) => {

    let updateProductStatus = {
        isActive : true
    }

    if (data.isAdmin) {

        return Product.findByIdAndUpdate(data.productId, updateProductStatus).then((product, error) => {

            if (error) {

                return false;

            } else {

                return true;

            }

        });

    }

    let message = Promise.resolve("You do not have the access rights to do this action!");

    return message.then(value => {

        return message;

    });

}
