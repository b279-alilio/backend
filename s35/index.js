const express = require("express");
const mongoose = require("mongoose");


const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.lydqzdg.mongodb.net/B279_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type: String,
		default: "pending"
	}
});

const userSchema = new mongoose.Schema({
	username : String,
	password : String
		
});

// Model name starts with capital letter -> "Task"
const Task = mongoose.model("Task", taskSchema)
const User = mongoose.model("User", userSchema)

// [SECTION] Creation of todo lists routes
// Setup for allowing the server to handle data from request
// Allows your app to read json data
// MIDDLEWARES

app.use(express.json());
// allows our app to read data from forms
app.use(express.urlencoded({extended:true}));

app.post("/tasks", (req, res) => {
	Task.findOne({ name : req.body.name }).then((result, err) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}
		else{
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
});

app.post("/signup", (req, res) => {
	User.findOne({ username : req.body.username }).then((result, err) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		}
		else{
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user registered");
				}
			})
		}
	})
});

app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if(err){
			return console.log(err);

		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
});

app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err){
			return console.log(err);

		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
});




if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app ;