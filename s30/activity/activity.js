db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: null, fruitsOnSale: {$sum: 1}}},
	{$project: {_id: 0}}
]);


db.fruits.aggregate([
	{$match: {stock: { $gte : 20 }}},
	{$group: {_id: null, enoughStock: {$sum: 1}}},
	{$project: {_id: 0}}
]);


db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
	{$sort: {avg_price: -1}}
]);


db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
	{$sort: {max_price: 1}}
]);


db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
	{$sort: {min_price: 1}}
]);
