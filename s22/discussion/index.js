// console.log("Hello Wolrd!");

// [SECTION] Array Methods
// JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
    - Mutator methods are functions that "mutate" or change an array after they're created
    - These methods manipulate the original array performing various tasks such as adding and removing elements
*/

// push()

/*
    - Adds an element in the end of an array AND returns the array's length
    - Syntax
        arrayName.push();
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple values
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()
/*
    - Removes the last element in an array AND returns the removed element
    - Syntax
        arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);

// unshift()
/*
    - Adds one or more elements at the beginning of an array
    - Syntax
        arrayName.unshift('elementA');
        arrayName.unshift('elementA', elementB);
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits);

// shift()
/*
    - Removes an element at the beginning of an array AND returns the removed element
    - Syntax
        arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

// splice()
/* 
    - Simultaneously removes elements from a specified index number and adds elements
    - Syntax
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method:");
console.log(fruits);

// sort()
/*
    - Rearranges the array elements in alphanumeric order
    - Syntax
        arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

// reverse()
/*
    - Reverses the order of array elements
    - Syntax
        arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);

// Non-mutator methods

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
/*
    - Returns the index number of the first matching element found in an array
    - If no match was found, the result will be -1.
    - The search process will be done from first element proceeding to the last element
    - Syntax
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log("Result from indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result from indexOf method: " + invalidCountry);

// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array
    - The search process will be done from last element proceeding to the first element
    - Syntax
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result from lastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 1)
console.log("Result from lastIndexOf method: " + lastIndexStart);

// slice()
/*
    - Portions/slices elements from an array AND returns a new array
    - Syntax
        arrayName.slice(startingIndex);
        arrayName.slice(startingIndex, endingIndex);
*/

// let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

// toString()
/*
    - Returns an array as a string separated by commas
    - Syntax
        arrayName.toString();
*/

let stringArr = countries.toString();
console.log("Result from toString method:");
console.log(stringArr);

// concat()
/*
    - Combines two arrays and returns the combined result
    - Syntax
        arrayA.concat(arrayB);
        arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("Result from concat method:");
console.log(tasks);

// Combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method:");
console.log(combinedTasks);

// join()
/*
    - Returns an array as a string separated by specified separator string
    - Syntax
        arrayName.join('separatorString');
*/

let users = ["John", "Jane", "joe", "Robert"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));


// Iteration
// forEach()
/*
    - Similar to a for loop that iterates on each array element.
    - For each item in the array, the anonymous function passed in the forEach() method will be run.
    - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
    - Variable names for arrays are normally written in the plural form of the data stored in an array
    - It's common practice to use the singular form of the array content for parameter names used in array loops
    - forEach() does not return anything.
    - Syntax
        arrayName.forEach(function(indivElement) {
            statement
        })
*/

tasks.forEach(function(task){
	console.log(task);
});

// Using forEAch with conditional statements

let filteredTasks = [];

tasks.forEach(function(task){
	if(task.length >= 10){
		filteredTasks.push(task);
	}
});

console.log("Result of filtered tasks:");
console.log(filteredTasks);

// map()
/* 
    - Iterates on each element AND returns new array with different values depending on the result of the function's operation
    - This is useful for performing tasks where mutating/changing the elements are required
    - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
    - Syntax
        let/const resultArray = arrayName.map(function(indivElement))
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
});

console.log("Original Array");
console.log(numbers);
console.log("Result from map method:");
console.log(numberMap);

// Using forEach

let numberForEach = numbers.forEach(function(number){
	return number * number;
});

console.log(numberForEach);

// every()
/*
    - Checks if all elements in an array meet the given condition
    - This is useful for validating data stored in arrays especially when dealing with large amounts of data
    - Returns a true value if all elements meet the condition and false if otherwise
    - Syntax
        let/const resultArray = arrayName.every(function(indivElement) {
            return expression/condition;
        })
*/

let allValid = numbers.every(function(number){
	return (number < 10);
});

console.log("Result of every method:");
console.log(allValid);

// some()
/*
    - Checks if at least one element in the array meets the given condition
    - Returns a true value if at least one element meets the condition and false if otherwise
    - Syntax
        let/const resultArray = arrayName.some(function(indivElement) {
            return expression/condition;
        })
*/

let someValid = numbers.some(function(number){
	return (number < -1);
});

console.log("Result of some method:");
console.log(someValid);

if(someValid){
	console.log("Some numbers in the array are greater than 2.");
}else{
	console.log("Some numbers in the array are not less than -1.");
}

// filter()
/*
    - Returns new array that contains elements which meets the given condition
    - Returns an empty array if no elements were found
    - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
    - Mastery of loops can help us work effectively by reducing the amount of code we use
    - Several array iteration methods may be used to perform the same result
    - Syntax
        let/const resultArray = arrayName.filter(function(indivElement) {
            return expression/condition;
        })
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
});

console.log("Result of filter method:");
console.log(filterValid);

// includes()
/*
    - includes() method checks if the argument passed can be found in the array.
    - it returns a boolean which can be saved in a variable.
        - returns true if the argument is found in the array.
        - returns false if it is not.
    - Syntax:
        arrayName.includes(<argumentToFind>)
*/

 let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

 let productFound1 = products.includes("Mouse");
 console.log(productFound1); //returns true

 let productFound2 = products.includes("Headset");
 console.log(productFound2); //returns false

// reduce()
/* 
    - Evaluates elements from left to right and returns/reduces the array into a single value
    - Syntax
        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
            return expression/operation
        })
    - The "accumulator" parameter in the function stores the result for every iteration of the loop
    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
    - How the "reduce" method works
        1. The first/result element in the array is stored in the "accumulator" parameter
        2. The second/next element in the array is stored in the "currentValue" parameter
        3. An operation is performed on the two elements
        4. The loop repeats step 1-3 until all elements have been worked on
*/

 let iteration = 0;

 let reducedArray = numbers.reduce(function(x, y){
 	// Used to track the current iteration count and accumulator data

 	console.warn("current iteration: " + ++iteration);
 	console.log("accumulator: " + x);
 	console.log("current value: " + x);

 	return x + y;
 });

console.log("Result of Reduce Method: " + reducedArray);

let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x, y){
	return x + "*" + y;
})

console.log("Result of Reduce Method: " + reducedJoin);















