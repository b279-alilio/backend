const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js")

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/tasks", taskRoute);

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.lydqzdg.mongodb.net/B279_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);











if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app ;
