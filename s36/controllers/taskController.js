const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
			if(error){
				console.log(error);
				return false;
			}
			else{
				return removedTask;
			}
		})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
			if(error){
				console.log(error);
				return false;
			}
			result.name = newContent.name;
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr){
					console.log(error);
					return false;
				}
				else{
					return updatedTask;
				}
			})
		})
}

module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
			if(error){
				console.log(error);
				return false;
			}
			result.status = newContent.status;
			return result.save().then((updatedStatus, saveErr) => {
				if(saveErr){
					console.log(error);
					return false;
				}
				else{
					return updatedStatus;
				}
			})
		})
}