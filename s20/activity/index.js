// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for(let x = 0; x < string.length; x++){
	if(
		string[x].toLowerCase() == "a" ||
		string[x].toLowerCase() == "e" ||
		string[x].toLowerCase() == "i" ||
		string[x].toLowerCase() == "o" ||
		string[x].toLowerCase() == "u" 
		){
			continue;
	} else{
		filteredString += string[x];
	}
}





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}