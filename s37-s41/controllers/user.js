const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/Course");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		}
		else{
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	  return newUser.save().then((user, error) => {
        if(error){
            return false;
        } else {
            return true;
        }
    })
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
				console.log(isPasswordCorrect)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false
			}
		}
	})
};

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

/*module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId});

		return course.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	if(isUserUpdated && isCourseUpdated){
		return "You are now enrolled!";
	}
	else{
		return "Something went wrong with your request. Please try again later!"
	}
}*/


module.exports.enroll = async (data, isAdmin) => {
	{
	console.log(isAdmin);

	if(isAdmin){
	
		return "ADMIN cannot do this action.";
	}
	else{
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.enrollments.push({courseId : data.courseId});
			return user.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			course.enrollees.push({userId : data.userId});

			return course.save().then((course, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		if(isUserUpdated && isCourseUpdated){
			return "You are now enrolled!";
		}
		else{
			return "Something went wrong with your request. Please try again later!"
		}
	}	
}
}