const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js")
const auth = require("../auth.js")


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

/*router.post("/enroll", (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId,
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});*/

router.post("/enroll", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId,
	}
	userController.enroll(data, isAdmin).then(resultFromController => res.send(resultFromController));
});






module.exports = router;