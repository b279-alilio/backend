const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
	if(request.url == "/login"){
		response.writeHead(220, {"Content-Type" : "text/plain"});
		response.end("This is the login page.");
	}else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("404: Page not found!");
	}
});

server.listen(port)
console.log(`Server now accessible at localhost:${port}`);